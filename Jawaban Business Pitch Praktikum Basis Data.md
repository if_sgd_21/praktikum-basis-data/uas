# UAS



## Menampilkan use case table untuk produk digitalnya
| Use Case | Actor |
| ------ | ------ |
| Pendaftaran Akun| User |
| Login | User |
| Update Akun | User |
| Follow Akun Pengguna Lain  | User |
| Menghapus Akun | User |
| Membuat Tweet  | User |
| Menampilkan Tweet       | User |
| Mengubah Tweet       | User |
| Menghapus Tweet       | User |
|Melihat Pertumbuhan Pengguna (perbulan)|Admin|
|Mengelompokkan Pengguna Berdasarkan Negara|Admin|
|Mengelompokkan Pengguna Berdasarkan Jenis Akun |Admin|
|Mengelompokkan Tweet Berdasarkan Jumlah Likes |Admin|
|Menganalisis Statistik Langganan Twitter |Admin|
|Mengatur Pelanggaran Tweet|Admin|

## Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya
- Screenshot koneksi ke database dari bahasa pemrograman yang dipilih:
![koneksi.php](assets/koneksi_php.png)
- Bentuk web servicenya:
![web-service-tweetify](assets/web-service-database.png)
## Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya
**Tools Visualisasi Data: Metabase**

Data Perbandingan Jumlah Akun Berdasarkan 3 Kategori (Regular, Premium, Admin)

![Data Tipe Akun](assets/Data_Jumlah_Akun_Berdasarkan_3_Kategori.png)

Data Perbandingan Jumlah Akun Berdasarkan Negara Asal

![Data Negara Pengguna](assets/Data_Jumlah_Pengguna_Berdasarkan_Negara.png)

Data Pertumbuhan Jumlah Akun (Diurutkan perbulan)

![Data Pertumbuhan](assets/Data_Jumlah_Pertumbuhan_Akun_Perbulan.png)

## Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan
- 1 Regex

Regex, atau Regular Expression, adalah sebuah pola pencarian dan manipulasi teks yang digunakan untuk mencocokkan dan memanipulasi string berdasarkan pola yang didefinisikan. Dalam regex, Anda dapat menggunakan berbagai karakter khusus, operator, dan sintaks untuk membangun pola yang kompleks dan fleksibel.
```sql
SELECT regexp_matches(first_name, 'Dian')
from "Users_detail";
-- Fungsi ini akan mengembalikan array hasil kecocokan jika ditemukan, 
-- Dan NULL jika tidak ada kecocokan.
```
- 1 Substring

Substring adalah sebuah operasi yang digunakan untuk mengambil bagian dari sebuah string berdasarkan posisi awal dan panjang yang ditentukan. Dalam banyak bahasa pemrograman, fungsi substring tersedia untuk memanipulasi string.
```sql
SELECT first_name, join_date,
    CASE
    -- Ternyata fungsi SUBSTRING tidak dapat langsung 
    -- digunakan dengan tipe data tanggal (date) dalam PostgreSQL.
    -- Untuk mendapatkan tahun dari kolom join_date 
    -- Kita perlu mengubahnya menjadi tipe data teks (varchar) terlebih dahulu sebelum menerapkan fungsi SUBSTRING. 
        WHEN SUBSTRING(CAST("join_date" AS VARCHAR), 1, 4) BETWEEN '2019' AND '2021' THEN 'Pengguna Lama'
        WHEN SUBSTRING(CAST("join_date" AS VARCHAR), 1, 4) BETWEEN '2022' AND '2023' THEN 'Pengguna Baru'
        ELSE '-'
    END AS Jenis_Pengguna
FROM "Users_detail";
```
- Sisanya bebas

Concat digunakan untuk menggabungkan dua atau lebih string menjadi satu string yang lebih panjang. Fungsi ini biasanya menerima parameter berupa string yang akan digabungkan.
```sql
SELECT CONCAT(first_name, ' ', last_name) as nama_lengkap 
FROM "Users_detail" 
GROUP BY nama_lengkap
ORDER BY nama_lengkap ASC;
```
Upper digunakan untuk mengubah semua karakter dalam sebuah string menjadi huruf kapital. Fungsi ini mengembalikan salinan string dengan semua karakter diubah menjadi huruf kapital. 
```sql
SELECT UPPER(country) from "Users_detail" GROUP BY country;

```

## Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya
Subquery (subquery dalam bahasa Indonesia juga sering disebut sebagai subkueri) adalah sebuah query yang tertanam di dalam query utama. Subquery digunakan untuk memperoleh data yang lebih spesifik atau mendapatkan hasil perhitungan yang kompleks dengan menggunakan data dari tabel yang terkait.

Subquery sering digunakan dalam pernyataan SELECT, INSERT, UPDATE, atau DELETE untuk memfilter, menggabungkan, atau menghitung data dengan cara yang lebih rumit. Subquery biasanya diletakkan dalam klausa WHERE, FROM, atau HAVING dari query utama.
```sql
-- Awalnya jumlah admin berkisar 30% lebih,
-- Lalu saya kurangi dengan limit 10
UPDATE "Users"
SET "acc_type" = 'admin'
WHERE "id" IN (
  SELECT "id"
  FROM "Users"
  WHERE "acc_type" = 'regular'
  LIMIT 10
);
```
## Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya
Transaksi adalah unit kerja dalam database yang terdiri dari satu atau lebih operasi yang harus dijalankan sebagai satu kesatuan. Properti ACID diterapkan dalam transaksi, yang berarti transaksi harus memenuhi atomisitas (semua operasi berhasil atau tidak sama sekali), konsistensi (database tetap konsisten sebelum dan setelah transaksi), isolasi (transaksi dijalankan terisolasi dari transaksi lain), dan daya tahan (perubahan yang dilakukan dalam transaksi permanen dan tahan terhadap kegagalan sistem). Transaksi penting untuk memastikan integritas data dan konsistensi dalam basis data.
```sql
BEGIN;
-- Insert subs monthly
INSERT INTO "Subscription_Plans" (user_id, price, level, start_date, end_date)
SELECT u.id, '30000', 'monthly', '2023-11-11 10:10:10', '2023-12-11 10:10:10'
FROM "Users" u
WHERE u.acc_type = 'premium';
-- Update subs annual untuk 10 akun pertama
UPDATE "Subscription_Plans"
SET price = '300000', level = 'annual', start_date = '2023-11-11 10:10:10', end_date = '2024-12-11 10:10:10'
WHERE "user_id" IN (
    SELECT u.id
    FROM "Users" u
    WHERE u.acc_type = 'premium'
    LIMIT 10
);

COMMIT;
-- Sepengalaman saya dalam melakukan transaksi, Saya mendapati error seperti:
--  ERROR: current transaction is aborted, commands ignored until end of transaction block
-- Cara mengatasinya adalah dengan memperbaiki syntax yang error, lalu jalankan:
ROLLBACK;
``` 
## Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya
Trigger adalah objek dalam database yang secara otomatis menjalankan perintah SQL ketika peristiwa tertentu terjadi pada tabel atau view. Peristiwa tersebut dapat berupa operasi INSERT, UPDATE, atau DELETE pada data. Trigger digunakan untuk merespons peristiwa tersebut dan melakukan tindakan yang diperlukan, seperti memperbarui data, memvalidasi input, atau memicu perubahan di tempat lain dalam database. Dengan menggunakan trigger, tindakan dapat dilakukan secara otomatis tanpa perlu menjalankan perintah SQL secara manual.

**Contoh Function:**
```sql
-- Fungsi Untuk Menghitung Followers Berdasarkan Username
CREATE OR REPLACE FUNCTION get_follower_count_by_username(username VARCHAR)
RETURNS INT
AS
$$
DECLARE
    follower_count INT;
BEGIN
    SELECT COUNT(id)
    INTO follower_count
    FROM "Follows"
    WHERE followed_id = (SELECT id FROM "Users" AS u WHERE u.username = get_follower_count_by_username.username);

    RETURN follower_count;
END;
$$
LANGUAGE plpgsql;
```
Sebelumnya cara menghitung followers dengan username adalah sebagai berikut.
```sql
SELECT username,
    -- Pake SubQuery karena table follows dan user terpisah
  (SELECT COUNT(id) FROM "Follows" WHERE followed_id = "Users"."id") AS followers
FROM "Users"
where username = 'chasegilang822';
```
Setelah membuat function tadi, kita bisa mendapatkan jumlah followers melalui query berikut.
```sql
SELECT get_follower_count_by_username(username) AS followers
FROM "Users"
WHERE username = 'chasegilang822';
```
**Contoh Trigger:**
Pertama, buatlah fungsi update_follower_count() yang mengembalikan tipe data TRIGGER. 
Fungsi ini akan dijalankan oleh trigger ketika terjadi operasi INSERT atau DELETE pada tabel "Follows".
```sql
CREATE OR REPLACE FUNCTION update_follower_count()
RETURNS TRIGGER
AS
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        -- Menambahkan 1 pada followers_count saat ada data baru di "Follows"
        UPDATE "Users"
        SET followers_count = followers_count + 1
        WHERE id = NEW.followed_id;
    ELSIF TG_OP = 'DELETE' THEN
        -- Mengurangi 1 dari followers_count saat ada data di "Follows" dihapus
        UPDATE "Users"
        SET followers_count = followers_count - 1
        WHERE id = OLD.followed_id;
    END IF;

    RETURN NULL;
END;
$$
LANGUAGE plpgsql;
```
Setelah mendefinisikan fungsi, buatlah trigger update_follower_count_trigger dengan menggunakan statement CREATE TRIGGER. Trigger ini akan dijalankan setelah operasi INSERT atau DELETE pada tabel "Follows" (AFTER INSERT OR DELETE). 

Untuk setiap baris yang terkena operasi tersebut (FOR EACH ROW), trigger akan menjalankan fungsi update_follower_count().
```sql
CREATE TRIGGER update_follower_count_trigger
AFTER INSERT OR DELETE ON "Follows"
FOR EACH ROW
EXECUTE FUNCTION update_follower_count();
```
Sebagai contoh, ini adalah query untuk menambahkan followers:
```sql
--  Following_id adalah user yang mengikuti
-- Followed_id adalah user yang diikuti
INSERT INTO "Follows" (id, following_id, followed_id) VALUES (150001, 1412, 11);
```
Lalu saat kita cek kolom followers_count pada table Users:
```sql
SELECT username, followers_count
FROM "Users" WHERE username = 'chasegilang822';
```
Maka hasilnya akan terupdate.
## Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya
DCL, singkatan dari Data Control Language, adalah bahasa yang digunakan untuk mengelola izin dan hak akses pengguna dalam basis data. DCL berfokus pada pengaturan hak akses, perizinan, dan kontrol keamanan dalam sistem basis data.

Langkah pertama yang kita lakukan adalah membuat akun beserta rolenya:

Referensi: [PostgreSQL Role](https://www.postgresql.org/docs/current/sql-createrole.html)
```sql
-- Membuat sebuah User bernama Dayen
CREATE ROLE Dayen;
-- Bisa juga sekalian opsi
CREATE ROLE Melodian WITH LOGIN PASSWORD 'mldn' CREATEDB;
-- Atau jika ingin menghapus bisa dengan:
DROP ROLE namaUser;
```
Jika lupa memberi opsi, gunakan alter role:
```sql
ALTER ROLE Dayen LOGIN PASSWORD 'apcb';
```
Sekarang kita perlu tambahkan hak aksesnya. Misalnya, untuk memberikan hak akses ke sebuah tabel, Anda dapat menggunakan perintah GRANT dengan format berikut:
```sql
GRANT <privileges> ON <table> TO <user/role>;
-- Memberikan hak akses SELECT ke semua tabel kepada User/Role "Dayen"
GRANT SELECT ON ALL TABLES IN SCHEMA public TO Dayen;
-- Memberikan hak akses INSERT, UPDATE, DELETE ke tabel "Users" kepada User/Role "Melodian"
GRANT INSERT, UPDATE, DELETE ON "Users" TO Melodian;
```
Selain memberikan hak akses, Kita juga dapat mencabut (revoke) hak akses menggunakan perintah REVOKE. Contoh penggunaan perintah REVOKE:
```sql
-- Mencabut hak akses SELECT dari tabel "employees" untuk peran "Dayen"
REVOKE SELECT ON ALL TABLES IN SCHEMA public FROM Dayen;
-- Mencabut hak akses INSERT, UPDATE, DELETE dari tabel "orders" untuk peran "Melodian"
REVOKE INSERT, UPDATE, DELETE ON "Users" FROM Melodian;
```
Untuk prakteknya, kita buka cmd untuk menjalankan postgre dengan ketentuan role/user demikian:
```cmd
psql --host=localhost --port=5432 --dbname=twitter --username=melodian --password
```
Lalu ketikkan password yang sudah dibuat tadi.
Ketika melakukan query select, maka akan muncul "ERROR:  permission denied for table Users".

Hal ini dikarenakan user/role Melodian hanya memiliki hak akses untuk membuat database dan melakukan DML (INSERT, UPDATE, dan DELETE).
## Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya:
Constraint (Ketentuan) dalam basis data adalah aturan yang diterapkan pada kolom atau tabel untuk membatasi nilai yang diperbolehkan atau memastikan integritas data. Ketentuan digunakan untuk menjaga konsistensi dan validitas data dalam basis data.

- Minimal 1 Unique Key

Unique key memungkinkan kolom atau kelompok kolom tersebut tidak memiliki duplikat nilai di dalam tabel.
```sql
CREATE TABLE "Users" (
 "id" INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
 "username" varchar UNIQUE NOT NULL, -- Unique Key
 "nickname" varchar,
 "email" varchar UNIQUE NOT NULL,
 "password" varchar NOT NULL,
 "acc_type" acc_types DEFAULT 'regular'
);
```
- Minimal 1 Foreign Key

Foreign key memungkinkan penggunaan relasi antara tabel, yang berguna untuk menggabungkan data dari tabel yang berbeda melalui operasi JOIN.
```sql
CREATE TABLE "Tweets" (
 "id" INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
 "user_id" int, -- Foreign Key
 "tweet_score" int,
 "tweet_text" TEXT NOT NULL,
 "date_created" DATE,
 "time_created" TIME
);
```
- Minimal 1 Index

Index berfungsi untuk mempercepat proses pencarian dan query. 
Minusnya adalah memperlambat proses manipulasi data (Insert, Update dan Delete).
```sql
-- Menambahkan index pada kolom acc_type tabel Users
CREATE INDEX acc_type_index ON "Users"(acc_type);
```
## Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
[Link Youtube](https://youtu.be/gxdAYHkbvb8)

## BONUS !!! Mendemonstrasikan UI untuk CRUDnya
Dimulai dari menit 29: [Link Youtube](https://youtu.be/gxdAYHkbvb8)
